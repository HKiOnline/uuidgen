package main

import (
    "code.google.com/p/go-uuid/uuid"
    "flag"
    "strings"
    "github.com/atotto/clipboard"
)

func main() {

	prefix := flag.String("prefix", "", "A prefix added before the UUID")
	postfix := flag.String("postfix", "", "A postfix added after the UUID")
	noDashes := flag.Bool("noDashes", false, "Remove dashes from the standard v. 4 UUID")
	pasteToClipboard := flag.Bool("paste", true, "Paste the UUID to clipboard")
	capitalize := flag.Bool("capitalize", false, "Capitalize the UUID")
	noOfUUIDs := flag.Int("count", 1, "Number of UUIDs generated")

	flag.Parse()

	uuids := ""

 	for index := 0; index < *noOfUUIDs; index++ {
        
		uuid := uuid.New()

		if *noDashes {
			uuid = strings.Replace(uuid, "-", "", -1)
		}

		if prefix != nil {
			uuid = *prefix + uuid
		}

		if postfix != nil {
			uuid = uuid + *postfix
		}

		if *capitalize {
			uuid = strings.ToUpper(uuid)
		}

		uuids = uuids + "\n" + uuid
		println(uuid)

	}


	if *pasteToClipboard {
		clipboard.WriteAll(uuids)
	}
}
