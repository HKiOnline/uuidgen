# UUIDGEN - UUID v.4 generator #

Simple utility program, used from the commandline. Generates version 4 UUIDs. Optionally you may add a string prefix and/or suffix to the UUID, remove the dashes and capitalize the UUID. You can also generate as many UUIDs as you wish.

The UUID(s) is/are copied automatically to clipboard so you don't have to copy&paste them from the commandline.

## Usage

	uuidge [-h | -capitalize | -count=<no of uuids> | -noDashes | -paste | -postfix=<postfix string> | -prefix=<prefix string>]

	Flags:
	 -h / -help: prints the flags below and usage
	 -capitalize=false: Capitalize the UUID
	 -count=1: Number of UUIDs generated
	 -noDashes=false: Remove dashes from the standard v. 4 UUID
	 -paste=true: Paste the UUID to clipboard
	 -postfix="": A postfix added after the UUID
	 -prefix="": A prefix added before the UUID


## Downloads
- [OS X 64bit binary](http://vcs.hkionline.net/uuidgen/downloads/uuidgen)
- [Windows executable](http://vcs.hkionline.net/uuidgen/downloads/uuidgen.exe)

The source is compilable in other operating systems as well. If you want to make sure you get the most up-to-date version, I suggest installing got, downloading this project and compiling it from source.